<?php

function newRef($link, $title) {
	global $references;
	$id = count($references);
	$id++;
	$references[] = array(
		'id'    => $id,
		'link'  => $link,
		'title' => $title,
	);
	return "<a id='see-reference-{$id}' href='#reference-{$id}' class='see reference'>[{$id}]</a>";
}
function print_references() {
	global $references;
	if (empty($references)) {
		return;
	}

	$h2_references = newH2();
	$h2_references['en'] = 'References';

	$out = '<ol id="reference-list">';
	foreach ($references AS $ref) {
		$out .= "<li id='reference-{$ref['id']}' class='reference' >";
		$out .=     "<a href='#see-reference-{$ref['id']}'>^</a> &nbsp;&nbsp;&nbsp;";
		$out .=     "<a href='{$ref['link']}' class='reference source' >{$ref['title']}</a>";
		$out .= "</li>";
	}
	$out .= '</ol>';

	$div_references = newSection();
	$div_references['id'] = "references";
	$div_references['en'] = $out;

	echo printH2($h2_references);
	echo printSection($div_references);
}
