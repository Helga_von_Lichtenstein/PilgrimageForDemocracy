<?php
	include_once('init.php');
	include_once('header.php');
	include_once('footer.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php print_title() ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta name='viewport'   content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
	<link rel="stylesheet" href="semantic/semantic.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="style/style.css" type="text/css" media="all" />

	<meta property="og:title"  content="<?php echo htmlspecialchars($h1['en']); ?>">
	<meta name="twitter:title" content="<?php echo htmlspecialchars($h1['en']); ?>">

	<meta name="description"         content="<?php echo htmlspecialchars($snp['description']); ?>">
	<meta property="og:description"  content="<?php echo htmlspecialchars($snp['description']); ?>">
	<meta name="twitter:description" content="<?php echo htmlspecialchars($snp['description']); ?>">

	<meta property="og:image"  content="<?php echo htmlspecialchars($snp['image']); ?>">
	<meta name="twitter:image" content="<?php echo htmlspecialchars($snp['image']); ?>">
	<meta name="twitter:card"  content="summary_large_image">
</head>

<body id="pagetop" class="">


<?php print_header() ?>

<?php print_h1() ?>

<?php if (!empty($body)) {echo $body;} ?>

<?php print_references() ?>

<?php print_footer() ?>


</body>
</html>
