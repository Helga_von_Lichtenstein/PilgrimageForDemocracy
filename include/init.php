<?php
	include_once('references.php');

$h1 = array(
	'id' => 'top',
	'class' => array('main-article'),
	'en' => 'Pilgrimage for Democracy',
);

$body = '';

// Social network preview
$snp = array(
	'description' => "Sowing the seeds of future peace",
	'image'       => '/copyrighted/hansjorg-keller-p7av1ZhKGBQ-1200-630.jpg',
);

$references = array();

function print_title() {
	global $h1;
	echo $h1['en'];
}

function print_h1() {
	global $h1;
	$class = join(' ', $h1['class']);

	echo '<h1 id="' . $h1['id'] . '" class="' . $class . '">';
	echo '<span lang="en">' . $h1['en'] . '</span>';
	echo '</h1>';
}

function newSection($type = NULL, $p1 = NULL, $p2 = NULL) {
	$section = array(
		'tag'   => 'div',
		'id'    => '',
		'class' => array('main-article'),
		'en'    => '',
		'stars' => -1,
			// -1: do not display.
			//  0: 4 grey stars   = not started.
			//  1: 1 red star     = stub.
			//  2: 2 orange stars = half written.
			//  3: 3 yellow stars = 1st draft completed / review / to complete.
			//  4: 4 green stars  = complete (but can be further improved if necessary).
	);

	if ($type == 'wikipedia') {
		$section['type']        = 'wikipedia';
		$section['link']['en']  = $p1;
		$section['title']['en'] = $p2;
		$section['class'][]     = 'wikipedia technical-section';
	}
	elseif ($type == 'codeberg') {
		$section['type']        = 'codeberg';
		$section['id']          = $p1;
		$section['title']['en'] = $p2;
		$section['class'][]     = 'codeberg technical-section';
	}

	return $section;
}


function newH2() {
	$h2 = newSection();
	$h2['tag'] = 'h2';
	return $h2;
}

function printSection($section) {
	$star_classes = array(
		-1 => "no-star-rating",
		0  => "star-rating zero-stars",
		1  => "star-rating one-star",
		2  => "star-rating two-stars",
		3  => "star-rating three-stars",
		4  => "star-rating four-stars"
	);
	if (isset($star_classes[$section['stars']])) {
		$section['class'][] = $star_classes[$section['stars']];
	}


	$class = join(' ', $section['class']);

	$out  = "";
	$out .= "<{$section['tag']} id='{$section['id']}' class='$class'>";
	$out .= "<div lang='en'>" . printSectionStars($section['stars']);

	if (isset($section['type'])) {
		if ($section['type'] == 'wikipedia') {
			$out .= '<i class="wikipedia w large icon"></i>';
			$out .= "<h3><a href='{$section['link']['en']}' class='wikipedia'>{$section['title']['en']}</a></h3>";
		}
		else if ($section['type'] == 'codeberg') {
			$out .= '<img class="ui tiny image codeberg" src="https://design.codeberg.org/logo-kit/icon_inverted.svg" width="26" height="26">';
			$out .= "<h3>Issue #{$section['id']}: &nbsp;&nbsp;&nbsp;";
			$out .= "<a href='https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/{$section['id']}' class='codeberg'>{$section['title']['en']}</a></h3>";
		}
	}

	$out .= "{$section['en']} </div>";
	$out .= "</{$section['tag']}>";

	return $out;
}

function printStars($nb) {
	$out = '';
	switch ($nb) {
		case 0:
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 1:
			$out .= "<i class='star red     icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 2:
			$out .= "<i class='star orange  icon'></i>";
			$out .= "<i class='star orange  icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 3:
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 4:
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			break;
	}
	return $out;
}

function printSectionStars($nb) {
	$out = '';
	if ($nb >= 0) {
		$out .= "<div class='section-stars'>";
		$out .= printStars($nb);
		$out .= "</div>";
	}
	return $out;
}

function printH2($h2) {
	return printSection($h2);
}

function addUpdate(&$div_update, $date, $stars_from, $stars_to, $link, $title_en, $text_en = '') {
	$update_en  = '<li>';
	$update_en .= "<strong>" . formatDateEn($date) . "</strong> &nbsp; ";
	$update_en .= printStars($stars_from);
	$update_en .= $stars_to >= 0 ? "<i class='right arrow icon'></i>" : '';
	$update_en .= printStars($stars_to);
	$update_en .= empty($link) ? '' : "<a href='$link'>$title_en</a> ";
	$update_en .= $text_en;
	$update_en .= '</li>';
	$div_update['en'] .= $update_en;
}

function formatDateEn($date) {
	return date("Y F jS", strtotime($date));
}

$div_stars = newSection();
$div_stars['class'][] = 'technical-section';
$div_stars['en'] = <<<HTML
	<p>
	<i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Planned article that has not been published yet.
	<br>
	<i class='star red     icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Stub article, there is not much to read yet.
	<br>
	<i class='star orange  icon'></i><i class='star orange  icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Article in progress. It may be only half written.
	<br>
	<i class='star yellow  icon'></i><i class='star yellow  icon'></i><i class='star yellow  icon'></i><i class='star outline icon'></i>
	Nearly completed article, waiting for reader feedback, or in need of some improvement.
	<br>
	<i class='star green   icon'></i><i class='star green   icon'></i><i class='star green   icon'></i><i class='star green   icon'></i>
	Completed article. It could still be further expanded and improved.
	</p>
	HTML;

$div_stub = newSection();
$div_stub['stars']   = -1;
$div_stub['class'][] = 'notice stub technical-section';
$div_stub['en'] = <<<HTML
	<div><i class="exclamation triangle large red icon"></i></div>
	<p><em>This article is a stub.
	You can help by expanding it.
	You can <a href="/participate.html">join</a> and
	<a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues">suggest improvements or edits</a> to this page.
	</em></p>
	HTML;
