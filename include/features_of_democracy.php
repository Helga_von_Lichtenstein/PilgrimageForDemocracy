<?php
include_once('include/init.php');

$div_what_is_democracy_primary_feature = newSection();
$div_what_is_democracy_primary_feature['stars'] = 3;
$div_what_is_democracy_primary_feature['en'] = <<<HTML
	<h3><a href="/primary_features_of_democracy.html">1: Primary features of democracy</a></h3>
	<p>This is the starting point in our journey towards a proper and complete understanding of what a democracy is.</p>
	<p>The etymological meaning is simple enough to comprehend, but the implications are easily overlooked.</p>
	<p>These primary features of democracy are fundamental.
	They are the basis upon which everything else is built.</p>
	<p>The primary flaws of democracy are:
		<span class="democracy flaw">individualism</span>,
		<span class="democracy flaw">selfishness</span>,
		<span class="democracy flaw">indifference</span>,
		<span class="democracy flaw">populism</span>.</p>
	<p>The primary virtues of democracy are:
		<span class="democracy virtue">freedom</span>,
		<span class="democracy virtue">peace</span>,
		<span class="democracy virtue">achievement</span>,
		<span class="democracy virtue">inspiration</span>,
		<span class="democracy virtue">aspiration</span>,
		<span class="democracy virtue">personal human development</span>,
		<span class="democracy virtue">personal spiritual development</span>.</p>
	HTML;

$div_what_is_democracy_secondary_feature = newSection();
$div_what_is_democracy_secondary_feature['stars'] = 0;
$div_what_is_democracy_secondary_feature['en'] = <<<HTML
	<h3>2: Secondary features of democracy</h3>
	<p>The primary features of democracy, if unmoderated and left unchecked, can lead to mob rule and the tyranny of the masses.
	The secondary features provide balance to the first ones.</p>
	<p>The secondary flaws of democracy are:
		<span class="democracy flaw">chaos</span>,
		<span class="democracy flaw">divisiveness</span>,
		<span class="democracy flaw">party politics</span>,
		<span class="democracy flaw">blaming others</span>,
		<span class="democracy flaw">pride</span>,
		<span class="democracy flaw">exploitation</span>.</p>
	<p>The secondary virtues of democracy are:
		<span class="democracy virtue">order</span>,
		<span class="democracy virtue">solidarity</span>,
		<span class="democracy virtue">justice</span>,
		<span class="democracy virtue">compassion</span>,
		<span class="democracy virtue">humility</span>,
		<span class="democracy virtue">cooperation</span>.</p>
	HTML;

$div_what_is_democracy_tertiary_feature = newSection();
$div_what_is_democracy_tertiary_feature['stars'] = 0;
$div_what_is_democracy_tertiary_feature['en'] = <<<HTML
	<h3>3: Tertiary features of democracy</h3>
	HTML;

$div_what_is_democracy_quarternary_feature = newSection();
$div_what_is_democracy_quarternary_feature['stars'] = 0;
$div_what_is_democracy_quarternary_feature['en'] = <<<HTML
	<h3>4: Quarternary features of democracy</h3>
	HTML;

$div_democracy_saints_and_little_devils = newSection();
$div_democracy_saints_and_little_devils['stars'] = 0;
$div_democracy_saints_and_little_devils['en'] = <<<HTML
	<h3>5: A thought experiment: democracy of saints vs democracy of little devils</h3>
	HTML;
