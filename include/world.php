<?php
include_once('include/init.php');

$div_democracy_world = newSection();
$div_democracy_world['stars']   = 0;
$div_democracy_world['class'][] = '';
$div_democracy_world['en'] = <<<HTML
	<h3><a href="/world.html">List of countries and international organizations</a></h3>

	<p>Although we are only getting started, we aim to progressively extend our coverage of countries around the world.</p>
	HTML;

$div_united_nations = newSection();
$div_united_nations['stars']   = 2;
$div_united_nations['class'][] = '';
$div_united_nations['en'] = <<<HTML
	<h3><a href="/united_nations.html">The United Nations</a></h3>

	<p>The United Nations has a mandate to maintain peace among nations.</p>
	HTML;

$div_osce = newSection();
$div_osce['stars']   = 0;
$div_osce['class'][] = '';
$div_osce['en'] = <<<HTML
	<h3><a href="/osce.html">OSCE (Organization for Security and Co-operation in Europe)</a></h3>

	<p>The OSCE's mandate includes issues such as arms control, promotion of human rights, freedom of the press, and free and fair elections.</p>
	HTML;

$div_prc_china = newSection();
$div_prc_china['stars']   = 0;
$div_prc_china['class'][] = '';
$div_prc_china['en'] = <<<HTML
	<h3><a href="/prc_china.html">The People's Republic of China</a></h3>

	<p>The People's Republic of China represents one of the most critical frontlines for democracy in the world.</p>
	HTML;

$div_iran = newSection();
$div_iran['stars']   = 1;
$div_iran['class'][] = '';
$div_iran['en'] = <<<HTML
	<h3><a href="/iran.html">Iran</a></h3>

	<p>Imposition of Islamic law, a continuing economic crisis, lack of freedom of expression,
	violation of women's rights, brutality carried out during protests, internet cutoffs,
	and the killing of Mahsa Amini were some of the reasons for the start of civil protests in Iran in 2021-2022.</p>
	HTML;

//$body .= printSection($div_democracy_world);
//$body .= printSection($div_united_nations);
//$body .= printSection($div_osce);
//$body .= printSection($div_prc_china);
//$body .= printSection($div_iran);
