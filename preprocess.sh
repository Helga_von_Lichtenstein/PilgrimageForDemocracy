#!/bin/bash

rm http/*.html


php src/index.php                                 > http/index.html
php src/menu.php                                  > http/menu.html
php src/updates.php                               > http/updates.html
php src/copyright.php                             > http/copyright.html
php src/participate.php                           > http/participate.html
php src/primary_features_of_democracy.php         > http/primary_features_of_democracy.html
php src/integrity.php                             > http/integrity.html
php src/fair_share.php                            > http/fair_share.html
php src/corruption.php                            > http/corruption.html
php src/media.php                                 > http/media.html
php src/justice.php                               > http/justice.html
php src/prc_china.php                             > http/prc_china.html
php src/chinese_expansionism.php                  > http/chinese_expansionism.html
php src/iran.php                                  > http/iran.html
php src/united_nations.php                        > http/united_nations.html
php src/world.php                                 > http/world.html
php src/osce.php                                  > http/osce.html
php src/costa_rica.php                            > http/costa_rica.html
