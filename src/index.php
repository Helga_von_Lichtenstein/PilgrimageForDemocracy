<?php
include_once('include/init.php');

$h1['en'] = 'Pilgrimage for Democracy and Social Justice';

$div_quote_purpose = newSection();
$div_quote_purpose['stars']   = -1;
$div_quote_purpose['class'][] = '';
$div_quote_purpose['en'] = <<<HTML
	<blockquote>
	"<strong>A pilgrim is a wanderer with a purpose.</strong>
	A pilgrimage can be to a place — that's the best-known kind — but it can also be for a thing.
	Mine is for peace, and that is why I am a Peace Pilgrim."
	<br>
	— Peace Pilgrim (1908 – 1981)
	</blockquote>
	HTML;

$div_democracy_and_social_justice = newSection();
$div_democracy_and_social_justice['stars']   = -1;
$div_democracy_and_social_justice['class'][] = '';
$div_democracy_and_social_justice['en'] = <<<HTML
	<h3>Democracy and social justice are two critical topics for our times</h3>

	<p>There cannot be true, lasting peace without democracy.
	<br>There cannot be true, lasting peace without social justice.
	</p>

	<p>The latest electronic gadgets, fancy cars, big houses, fashionable clothes, etc., are not critical necessities.
	But simple vegetables or a simple bowl of rice are essential items for life.
	<br><strong>A world where many have much too much
	while too many still struggle to survive with the barest necessities of life,
	cannot achieve global peace.</strong>
	</p>

	<p>Democracy and social justice are the two sides of the same coin.
	</p>
	HTML;

$div_improve_ourselves_institutions_leaders = newSection();
$div_improve_ourselves_institutions_leaders['stars']   = -1;
$div_improve_ourselves_institutions_leaders['class'][] = '';
$div_improve_ourselves_institutions_leaders['en'] = <<<HTML
	<blockquote>
	"<strong>In order for the world to become peaceful, people must become more peaceful</strong>.
	Among mature people war would not be a problem — it would be impossible.
	In their immaturity people want, at the same time, peace and the things which make war.
	However, people can mature just as children grow up.
	Yes, our institutions and our leaders reflect our immaturity,
	but <strong>as we mature we will elect better leaders and set up better institutions</strong>.
	It always comes back to the thing so many of us wish to avoid: working to improve ourselves.
	<br>
	— Peace Pilgrim (1908 – 1981), Harmonious Principles for Human Living.
	</blockquote>
	HTML;

$h2_menu = newH2();
$h2_menu['en'] = 'Come on in!';

$div_menu = newSection();
$div_menu['stars'] = -1;
$div_menu['en'] = <<<HTML
	<h3><a href="/menu.html">Menu</a></h3>
	<p>There are so many topics to cover!
	Check the growing listing of sections and articles that are already published.
	You can also preview some of the topics that we plan to develop in the next few months and years.</p>
	<p>The menu provides a convenient entry point for new and returning visitors.</.</p>
	HTML;

$div_participate = newSection();
$div_participate['stars'] = -1;
$div_participate['en'] = <<<HTML
	<h3><a href="/participate.html">Participate</a></h3>
	<p>Let's pool our resources, knowledge and skills.
	Teamwork and our collective intelligence will ensure the success of this project.</p>
	<p>Share all around you, all that which inspires you.</p>
	<p>Together, let's try to inspire people around us to sow the seeds of future peace.</p>
	HTML;

$div_copyright = newSection();
$div_copyright['stars'] = -1;
$div_copyright['en'] = <<<HTML
	<h3><a href="/copyright.html">Copyright?</a></h3>
	<p>The best ideas for a mature democracy and social justice do not belong to any single person or organization.
	They belong to all of humanity.</p>
	<p>Most of the content on this website is placed in the public domain.
	You can share, copy, duplicate, republish, modify all of the most important articles,
	and add your best ideas, research, and policy proposals.</p>
	<p>There are, however, some minor exceptions for third-party material and code.
	You can check this page for details.</p>
	HTML;

$div_updates = newSection();
$div_updates['stars'] = -1;
$div_updates['en'] = <<<HTML
	<h3><a href="/updates.html">Updates</a></h3>
	<p>If you are already very familiar with the existing content of this website,
	this page makes it convenient to check all the newest updates and additions since your last visit.</p>
	HTML;



$body .= printSection($div_quote_purpose);
$body .= printSection($div_democracy_and_social_justice);
$body .= printSection($div_improve_ourselves_institutions_leaders);
$body .= printH2($h2_menu);
$body .= printSection($div_menu);
$body .= printSection($div_participate);
$body .= printSection($div_copyright);
$body .= printSection($div_updates);

include('include/page.php');
