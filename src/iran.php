<?php
include_once('include/world.php');
$h1['en'] = 'Iran';


$div_codeberg = newSection('codeberg', '15', 'Democracy and Human Rights in Iran');
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>Let us know how to improve this page.</p>
	HTML;



$h2_iran_today = newH2();
$h2_iran_today['en'] = 'Iran today';

$h2_iran_democracy = newH2();
$h2_iran_democracy['en'] = 'Iran and democracy';

$h2_iran_human_rights = newH2();
$h2_iran_human_rights['en'] = 'Iran and human rights';




$div_wikipedia_human_rights_islamic_republic_iran = newSection('wikipedia',
                                                               'https://en.wikipedia.org/wiki/Human_rights_in_the_Islamic_Republic_of_Iran',
							       'Human rights in the Islamic Republic of Iran');
$div_wikipedia_human_rights_islamic_republic_iran['stars']   = -1;
$div_wikipedia_human_rights_islamic_republic_iran['class'][] = '';
$div_wikipedia_human_rights_islamic_republic_iran['en'] = <<<HTML
	<p>This article is only about human rights in Iran, specifically since 1979.</p>
	HTML;

$div_wikipedia_human_rights_in_iran = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Human_rights_in_Iran', 'Human rights in Iran');
$div_wikipedia_human_rights_in_iran['stars']   = -1;
$div_wikipedia_human_rights_in_iran['class'][] = '';
$div_wikipedia_human_rights_in_iran['en'] = <<<HTML
	<p>Human rights in Iran during
	the Imperial Pahlavi dynasty (1925 to 1979),
	and the Islamic Republic (since 1979).
	</p>
	HTML;



$div_wikipedia_history_democracy_classical_iran = newSection('wikipedia', 'https://en.wikipedia.org/wiki/History_of_democracy_in_classical_Iran', 'History of democracy in classical Iran');
$div_wikipedia_history_democracy_classical_iran['stars']   = -1;
$div_wikipedia_history_democracy_classical_iran['class'][] = '';
$div_wikipedia_history_democracy_classical_iran['en'] = <<<HTML
	<p>Covers Zoroastrianism,
	the Medes (around 11th century BCE),
	the Achaemenid (9th century BCE),
	and the Arsacid Empire (3rd century BCE).
	</p>
	HTML;



$div_wikipedia_iran_human_rights_org = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Iran_Human_Rights', 'Iran Human Rights');
$div_wikipedia_iran_human_rights_org['stars']   = -1;
$div_wikipedia_iran_human_rights_org['class'][] = '';
$div_wikipedia_iran_human_rights_org['en'] = <<<HTML
	<p>Iran Human Rights (IHR) (Persian: سازمان حقوق بشر ایران)
	is a non-profit international non-governmental organization focused on human rights in Iran.
	Founded in 2005, it is a non-partisan and politically independent organisation based in Oslo, Norway.
	</p>

	<p>Official web site: <a href="https://www.iranhr.net/en/">iranhr.net - Iran Human Rights</a>.</p>
	HTML;


$div_wikipedia_islam_and_democracy = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Islam_and_democracy', 'Islam and democracy');
$div_wikipedia_islam_and_democracy['stars']   = -1;
$div_wikipedia_islam_and_democracy['class'][] = '';
$div_wikipedia_islam_and_democracy['en'] = <<<HTML
	<p>There exist several perspectives on the relationship between Islam and democracy
	among Islamic political theorists, the general Muslim public, and Western authors.</p>

	<p>Polls indicate that majorities in the Muslim world desire a political model where
	democratic institutions and values can coexist with the values and principles of Islam, seeing no contradiction between the two.</p>
	HTML;


$div_wikipedia_winds_of_change = newSection('wikipedia',
                                            'https://en.wikipedia.org/wiki/Winds_of_Change:_The_Future_of_Democracy_in_Iran',
					    'Winds of Change: The Future of Democracy in Iran');
$div_wikipedia_winds_of_change['stars']   = -1;
$div_wikipedia_winds_of_change['class'][] = '';
$div_wikipedia_winds_of_change['en'] = <<<HTML
	<p><em>Winds of Change: The Future of Democracy in Iran</em> is a book written by Prince Reza Pahlavi II, Crown Prince of Iran.
	He advocates the principles of freedom, democracy and human rights for his countrymen.</p>
	HTML;


$div_wikipedia_children_rights_iran = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Childrens%27_Rights_in_Iran', "Childrens' Rights in Iran");
$div_wikipedia_children_rights_iran['stars']   = -1;
$div_wikipedia_children_rights_iran['class'][] = '';
$div_wikipedia_children_rights_iran['en'] = <<<HTML
	<p>Despite having signed the UN Convention on the Rights of the Child (CRC) in 1991 and ratified it in 1994,
	Iran has not upheld its obligations under the treaty.</p>
	HTML;


$div_wikipedia_defenders_human_rights = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Defenders_of_Human_Rights_Center', 'Defenders of Human Rights Center');
$div_wikipedia_defenders_human_rights['stars']   = -1;
$div_wikipedia_defenders_human_rights['class'][] = '';
$div_wikipedia_defenders_human_rights['en'] = <<<HTML
	<p>The Defenders of Human Rights Center is an Iranian human rights organization.
	Based in Tehran, the organization was founded in 2001 and
	has actively defended the rights of women, political prisoners and minorities in Iran.</p>

	<p>Official web site: <a href="https://www.humanrights-ir.org/">humanrights-ir.org - Defenders of Human Rights Center</a>.</p>
	HTML;


$div_wikipedia_foundation_democracy_iran = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Foundation_for_Democracy_in_Iran', 'Foundation for Democracy in Iran');
$div_wikipedia_foundation_democracy_iran['stars']   = -1;
$div_wikipedia_foundation_democracy_iran['class'][] = '';
$div_wikipedia_foundation_democracy_iran['en'] = <<<HTML
	<p>The Foundation for Democracy in Iran is a private, non-profit organization established in 1995
	with grants from the National Endowment for Democracy (NED), to promote regime change in Iran.</p>

	<p>Official web site: <a href="http://www.iran.org/">iran.org - Foundation for Democracy in Iran</a>.</p>
	HTML;




$div_wikipedia_2021_2023_iranian_protests = newSection('wikipedia', 'https://en.wikipedia.org/wiki/2021%E2%80%932023_Iranian_protests', '2021–2023 Iranian protests');
$div_wikipedia_2021_2023_iranian_protests['stars']   = -1;
$div_wikipedia_2021_2023_iranian_protests['class'][] = '';
$div_wikipedia_2021_2023_iranian_protests['en'] = <<<HTML
	<p>Imposition of Islamic law, a continuing economic crisis, lack of freedom of expression, violation of women's rights,
	brutality carried out during protests, internet cutoffs, and the killing of Mahsa Amini
	were some of the reasons for the start of civil protests in Iran in 2021-2022.</p>
	HTML;

$div_wikipedia_mahsa_amini_protests = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Mahsa_Amini_protests', 'Mahsa Amini protests');
$div_wikipedia_mahsa_amini_protests['stars']   = -1;
$div_wikipedia_mahsa_amini_protests['class'][] = '';
$div_wikipedia_mahsa_amini_protests['en'] = <<<HTML
	<p>Civil unrest and protests against the government of Iran
	associated with the death in police custody of Mahsa Amini (Persian: مهسا امینی)
	began on 16 September 2022 and are ongoing in 2023.</p>
	HTML;





$body .= printSection($div_democracy_world);
$body .= printSection($div_stub);
$body .= printSection($div_codeberg);

$body .= printH2($h2_iran_today);
$body .= printSection($div_wikipedia_human_rights_islamic_republic_iran);
$body .= printSection($div_wikipedia_2021_2023_iranian_protests);
$body .= printSection($div_wikipedia_mahsa_amini_protests);


$body .= printH2($h2_iran_human_rights);
$body .= printSection($div_wikipedia_human_rights_in_iran);
$body .= printSection($div_wikipedia_children_rights_iran);
$body .= printSection($div_wikipedia_iran_human_rights_org);
$body .= printSection($div_wikipedia_defenders_human_rights);

$body .= printH2($h2_iran_democracy);
$body .= printSection($div_wikipedia_history_democracy_classical_iran);
$body .= printSection($div_wikipedia_islam_and_democracy);
$body .= printSection($div_wikipedia_winds_of_change);
$body .= printSection($div_wikipedia_foundation_democracy_iran);



include('include/page.php');
