<?php
include_once('include/init.php');
$h1['en'] = 'Updates';

$h2_code = newH2();
$h2_code['en'] = 'Code update';

$div_code = newSection();
$div_code['stars'] = -1;
$div_code['class'][] = '';
$div_code['en'] = <<<HTML
	<p>This web site is open source.
	The whole source code is published within the <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy">Pilgrimage for Democracy</a> at Codeberg.
	If you are interested in following every minor change in the website,
	you can browse the project's code repository and <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master">check every single commit</a>.
	</p>
	<p>See below for the most important content updates, with new articles, expanded articles and expanded sections.</p>
	HTML;

$h2_content = newH2();
$h2_content['en'] = 'Content update';

$div_update = newSection();
$div_update['en'] = <<<HTML
	<p>See below the list of <em>major</em> updates since your last visit:</p>
	<ul>
	HTML;

// function addUpdate(&$div_update, $date, $stars_from, $stars_to, $link, $title_en, $text_en = '')
//addUpdate($div_update, '2023-01-2', 0, 1, '/.html', '', '');
addUpdate($div_update, '2023-01-28', 0, 0, '/osce.html', 'OSCE (Organization for Security and Co-operation in Europe)', '');
addUpdate($div_update, '2023-01-27', 0, 2, '/united_nations.html', 'The United Nations', '');
addUpdate($div_update, '2023-01-22', 0, 1, '/iran.html', 'Iran', 'Start with resources from wikipedia.');
addUpdate($div_update, '2022-12-26', 0, 1, '/chinese_expansionism.html', 'Chinese expansionism', '');
addUpdate($div_update, '2022-12-12', 1, 2, '/participate.html', 'Participate', 'Content');
addUpdate($div_update, '2022-12-08', 1, 3, '/primary_features_of_democracy.html', 'Primary features of democracy', '');
addUpdate($div_update, '2022-11-26', 0, 0, '/justice.html', 'Justice', '');
addUpdate($div_update, '2022-11-25', 0, 0, '/media.html', 'Media', '');
addUpdate($div_update, '2022-11-25', 0, 1, '/corruption.html', 'Corruption', '');
addUpdate($div_update, '2022-11-23', 0, 1, '/fair_share.html', 'Fair share', '');
addUpdate($div_update, '2022-11-20', 0, 1, '/integrity.html', 'Election integrity', '');
addUpdate($div_update, '2022-11-14', 0, 1, '/primary_features_of_democracy.html', 'Primary features of democracy', '');
addUpdate($div_update, '2022-11-13', 0, 1, '/participate.html', 'Participate', '');
addUpdate($div_update, '2022-11-12', 0, 3, '/copyright.html', 'Copyright?', '');
addUpdate($div_update, '2022-11-11', 0, 1, '/updates.html', 'Updates');
addUpdate($div_update, '2022-11-09', 0, 1, '/index.html', 'Front page');
addUpdate($div_update, '2022-11-05', -1, -1, '', '', 'Beginning of the project. Resgistration of the domain pildem.org.');

$div_update['en'] .= "</ul>";




$body .= printH2($h2_code);
$body .= printSection($div_code);

$body .= printH2($h2_content);
$body .= printSection($div_stars);
$body .= printSection($div_update);

include('include/page.php');
