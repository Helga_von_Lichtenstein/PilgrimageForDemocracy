<?php
include_once('include/init.php');
$h1['en'] = 'Media';

$h2_introduction = newH2();
$h2_introduction['en'] = 'Introduction';

$div_introduction = newSection();
$div_introduction['stars']   = 0;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>The quality of our knowledge of public matters is commensurate with the quality of the media that deliver us the information
	upon which we rely to create our own opinion of what is right and what is wrong, whom to vote for or against, etc.</p>

	<p>On one side, we support free speech, the plurality of opinions and systemic checks and balances,
	with the media acting as a counterweight to political and economic powers.<p>

	<p>On the other side, we must find remedies against the excesses of unrestrained free speech, fake news, manipulative media,
	money politics, and media controlled by powerful vested interests.</p>

	<p>We must find a way to strike a balance between these apparently contradictory injunctions.</p>

	<p>How can we, collectively, become media-savvy, foster reliable and trustworthy media, etc.?</p>
	HTML;

$body .= printSection($div_stub);
$body .= printH2($h2_introduction);
$body .= printSection($div_introduction);


include('include/page.php');
