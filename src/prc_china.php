<?php
include_once('include/world.php');

$h1['en'] = "The People's Republic of China";

$div_chinese_expansionism = newSection();
$div_chinese_expansionism['stars']   = 1;
$div_chinese_expansionism['class'][] = '';
$div_chinese_expansionism['en'] = <<<HTML
	<h3><a href="/chinese_expansionism.html">Chinese expansionism</a></h3>

	<p>The People's Republic of China has territorial disputes with most of its neighbors.
	The PRC government has a long term policy to gain control over territories it considers its own.</p>
	HTML;

$div_codeberg = newSection('codeberg', '12', "Research the People's Republic of China's claims to being a democracy");
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>The PRC is pushing the idea that the PRC represents a Chinese-style democracy.
	We need to analyze and deconstruct the claim.</p>
	HTML;

$div_wikipedia_china_democracy = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Democracy_in_China', 'Democracy in China');
$div_wikipedia_china_democracy['stars']   = -1;
$div_wikipedia_china_democracy['class'][] = '';
$div_wikipedia_china_democracy['en'] = <<<HTML
	<p>The Constitution of the People's Republic of China (PRC) states that its form of government is a "people's democratic dictatorship".
	The CCP says that China is a "socialist democracy", in which the CCP is the central authority and acts in the people's interest.</p>
	HTML;

$div_wikipedia_china_human_right = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Human_rights_in_China', 'Human rights in China');
$div_wikipedia_china_human_right['stars']   = -1;
$div_wikipedia_china_human_right['class'][] = '';
$div_wikipedia_china_human_right['en'] = <<<HTML
	<p>A long, well-documented Wikipedia article on the Human Rights situation in the People's Republic of China.</p>
	HTML;

$r1 = newRef('https://news.ltn.com.tw/news/world/breakingnews/4191842', '白紙運動秋後算帳！小粉紅竟嗆「重判別放出來」 網嘆：沒救了...');

$div_wikipedia_2022_covid_protests_china = newSection('wikipedia', 'https://en.wikipedia.org/wiki/2022_COVID-19_protests_in_China', '2022 COVID-19 protests in China');
$div_wikipedia_2022_covid_protests_china['stars']   = -1;
$div_wikipedia_2022_covid_protests_china['class'][] = '';
$div_wikipedia_2022_covid_protests_china['en'] = <<<HTML
	<p>A series of protests against COVID-19 lockdowns began in mainland China in November 2022.
	Colloquially referred to as the White Paper Protests (白纸抗议) or the A4 Revolution (白纸革命),
	the demonstrations started in response to measures taken by the Chinese government to prevent the spread of COVID-19 in the country,
	including implementing a zero-COVID policy.</p>

	<p>It is reported that in the weeks and months following the protests,
	the Chinese authorities started to round up and arrest people who had participated in the protests. {$r1}</p>
	HTML;



$body .= printSection($div_democracy_world);
$body .= printSection($div_stub);
$body .= printSection($div_chinese_expansionism);
$body .= printSection($div_codeberg);

$body .= printSection($div_wikipedia_china_democracy);
$body .= printSection($div_wikipedia_china_human_right);
$body .= printSection($div_wikipedia_2022_covid_protests_china);

include('include/page.php');
