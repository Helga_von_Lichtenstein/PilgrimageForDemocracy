<?php
include_once('include/features_of_democracy.php');
include_once('include/world.php');

$h1['en'] = 'Menu';


$h2_what_is_democracy = newH2();
$h2_what_is_democracy['en'] = 'What is democracy?';

$div_what_is_democracy = newSection();
$div_what_is_democracy['en'] = <<<HTML
	<h3>Definition of Democracy</h3>

	<p>Before we can even start discussing the ways our democracy can be improved,
	we must agree on a common sense definition of the term.</p>

	<p>Searching how countless thinkers and scholars define the word "democracy",
	we can't fail to notice that there is no simple, standard definition.
	Similarly, if we were to ask common people on the street to define democracy, we would get a variety of different answers
	and we would be no closer to having an exact definition of the term.</p>

	<p>However, by putting together all the possible answers and definitions,
	we can highlight the intrinsic qualities of democracies.
	A definite series of features stand out,
	with each one deriving from the previously enumerated ones.</p>

	<p>It is, after all, critical to gain a clear understanding of the defining features of a democracy,
	because it provides a bedrock upon which to build all of that which will be discussed afterwards,
	a standard that we can use to gauge our democratic achievements.</p>
	HTML;


$h2_priorities_avoid = newH2();
$h2_priorities_avoid['en'] = 'Challenges';

$div_priorities_avoid = newSection();
$div_priorities_avoid['en'] = <<<HTML
	<p>This section explores the main challenges in established democracies.</p>
	HTML;

$div_duverger_syndrome = newSection();
$div_duverger_syndrome['stars'] = 0;
$div_duverger_syndrome['en'] = <<<HTML
	<h3>1: Duverger Syndrome</h3>
	HTML;

$div_tweed_syndrome = newSection();
$div_tweed_syndrome['stars'] = 0;
$div_tweed_syndrome['en'] = <<<HTML
	<h3>2: Tweed Syndrome</h3>
	HTML;

$h2_priorities_build = newH2();
$h2_priorities_build['en'] = 'Building democracy';

$div_election_method = newSection();
$div_election_method['stars']   = 0;
$div_election_method['class'][] = '';
$div_election_method['en'] = <<<HTML
	<h3>1: Good election method</h3>
	HTML;

$div_integrity = newSection();
$div_integrity['stars']   = 1;
$div_integrity['class'][] = '';
$div_integrity['en'] = <<<HTML
	<h3><a href="/integrity.html">2: Integrity</a></h3>
	HTML;

$div_build_good_media = newSection();
$div_build_good_media['stars']   = 0;
$div_build_good_media['class'][] = '';
$div_build_good_media['en'] = <<<HTML
	<h3>3: Good media environment</h3>
	HTML;


$h2_taxes = newH2();
$h2_taxes['en'] = 'Taxes';

$div_taxes = newSection();
$div_taxes['stars'] = 0;
$div_taxes['class'][] = '';
$div_taxes['en'] = <<<HTML
	HTML;

$h2_freedom_of_speech = newH2();
$h2_freedom_of_speech['en'] = 'Freedom of speech';


$div_freedom_of_speech = newSection();
$div_freedom_of_speech['stars']   = 0;
$div_freedom_of_speech['class'][] = '';
$div_freedom_of_speech['en'] = <<<HTML
	HTML;

$h2_media = newH2();
$h2_media['en'] = 'Media';


$div_media = newSection();
$div_media['stars']   = 0;
$div_media['class'][] = '';
$div_media['en'] = <<<HTML
	<h3><a href="/media.html">Media</a></h3>
	HTML;

$h2_institutions = newH2();
$h2_institutions['en'] = 'Institutions';

$div_institutions = newSection();
$div_institutions['stars']   = 0;
$div_institutions['class'][] = '';
$div_institutions['en'] = <<<HTML
	HTML;

$div_corruption = newSection();
$div_corruption['stars']   = 0;
$div_corruption['class'][] = '';
$div_corruption['en'] = <<<HTML
	<h3><a href="/corruption.html">Corruption</a></h3>

	<p>The continued failure of most countries to significantly control corruption
	is contributing to a crisis in democracy around the world.</p>
	HTML;


$h2_justice = newH2();
$h2_justice['en'] = 'Justice';


$div_justice = newSection();
$div_justice['stars']   = 0;
$div_justice['class'][] = '';
$div_justice['en'] = <<<HTML
	<h3><a href="/justice.html">Justice</a></h3>
	HTML;

$h2_political_discourse = newH2();
$h2_political_discourse['en'] = 'Political discourse';


$div_political_discourse = newSection();
$div_political_discourse['stars']   = 0;
$div_political_discourse['class'][] = '';
$div_political_discourse['en'] = <<<HTML
	HTML;

$h2_fair_share = newH2();
$h2_fair_share['en'] = 'Fair share';

$div_fair_share = newSection();
$div_fair_share['stars']   = 0;
$div_fair_share['class'][] = '';
$div_fair_share['en'] = <<<HTML
	<h3><a href="/fair_share.html">Fair share</a></h3>
	HTML;

$h2_democracy_world = newH2();
$h2_democracy_world['en'] = 'Democracy and social justice in the world';


$body .= printSection($div_stars);

$body .= printH2($h2_what_is_democracy);
$body .= printSection($div_what_is_democracy);
$body .= printSection($div_what_is_democracy_primary_feature);
$body .= printSection($div_what_is_democracy_secondary_feature);
$body .= printSection($div_what_is_democracy_tertiary_feature);
$body .= printSection($div_what_is_democracy_quarternary_feature);
$body .= printSection($div_democracy_saints_and_little_devils);

$body .= printH2($h2_priorities_avoid);
$body .= printSection($div_priorities_avoid);
$body .= printSection($div_duverger_syndrome);
$body .= printSection($div_tweed_syndrome);

$body .= printH2($h2_priorities_build);
$body .= printSection($div_election_method);
$body .= printSection($div_integrity);

$body .= printH2($h2_taxes);
$body .= printSection($div_taxes);

$body .= printH2($h2_freedom_of_speech);
$body .= printSection($div_freedom_of_speech);

$body .= printH2($h2_media);
$body .= printSection($div_media);

$body .= printH2($h2_institutions);
$body .= printSection($div_institutions);
$body .= printSection($div_corruption);

$body .= printH2($h2_justice);
$body .= printSection($div_justice);

$body .= printH2($h2_political_discourse);
$body .= printSection($div_political_discourse);


$body .= printH2($h2_fair_share);
$body .= printSection($div_fair_share);

$body .= printH2($h2_democracy_world);
$body .= printSection($div_democracy_world);
$body .= printSection($div_united_nations);
$body .= printSection($div_iran);


include('include/page.php');
