<?php
include_once('include/init.php');
$h1['en'] = 'Justice';

$h2_introduction = newH2();
$h2_introduction['en'] = 'Introduction';

$div_introduction = newSection();
$div_introduction['stars']   = -1;
$div_introduction['class'][] = '';
$div_introduction['en'] = <<<HTML
	<p>A strong judiciary is important in any democracy,
	as it can balance the powers of the executive and of the legislative.</p>

	<p>Topics that can be covered are:
	judge appointments and terms,
	independence of the judges and the judiciary,
	as well as the potential corruption of the system and of individual judges.</p>
	HTML;

$h2_criminal_justice = newH2();
$h2_criminal_justice['en'] = 'Criminal justice';

$div_criminal_justice = newSection();
$div_criminal_justice['stars']   = -1;
$div_criminal_justice['class'][] = '';
$div_criminal_justice['en'] = <<<HTML
	<p>Criminal justice should pursue the following goals, in order of importance:
	</p>
	<ol>
		<li>Protect society from dangerous criminals (by locking them up, etc.).</li>
		<li>Considering criminals as diseased individuals,
		provide the opportunity to heal them so they can become integrated, productive members of society.</li>
		<li>Give proportional punishment for a given crime.</li>
	</ol>
	HTML;

$div_wikipedia_life_imprisonment = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Life_imprisonment', 'Life imprisonment');
$div_wikipedia_life_imprisonment['stars']   = -1;
$div_wikipedia_life_imprisonment['class'][] = '';
$div_wikipedia_life_imprisonment['en'] = <<<HTML
	<p>Most countries have an effective live sentence, lasting until the convict's natural death.
	Portugal was the first country to abolish life imprisonment in 1884.
	In Norway, the maximum sentence is 21 years.</p>
	HTML;

$div_codeberg = newSection('codeberg', '14', 'Nepal: What is the maximum duration of a life sentence?');
$div_codeberg['stars']   = -1;
$div_codeberg['class'][] = '';
$div_codeberg['en'] = <<<HTML
	<p>Different sources give the life sentence in Nepal as 20 years, 25 years, or an effective life sentence.</p>
	HTML;


$body .= printSection($div_stub);
$body .= printH2($h2_introduction);
$body .= printSection($div_introduction);

$body .= printH2($h2_criminal_justice);
$body .= printSection($div_criminal_justice);
$body .= printSection($div_codeberg);
$body .= printSection($div_wikipedia_life_imprisonment);




include('include/page.php');
