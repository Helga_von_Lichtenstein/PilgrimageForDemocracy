<?php
include_once('include/init.php');
$h1['en'] = 'Election integrity';

$div_integrity = newSection();
$div_integrity['en'] = <<<HTML
	<p>In an age when there is so much defiance over the electoral process,
	in a society where populist candidates thrive on casting doubts about the integrity of the vote count,
	it is critical that we establish an election protocol beyond any doubt.</p>

	<p>The goal is not so much to rely on experts and election officials who would assure us that massive voter fraud did not occur,
	or indeed, that it is almost impossible.
	The goal is to create such a foolproof system that any well-intentioned voter can witness the integrity of the entire process with their own eyes.
	The chain of custody of cast ballots should be under constant voter supervision.</p>

	<p>This way, should anyone believe that some elections were rigged,
	they could be simply invited to stay the whole day at the polling station,
	and witness first the whole electoral process and vote count from beginning to end.
	Any doubt in their mind could then easily be dispelled.</p>

	<p>The purpose of this section is to clearly establish the protocol and conditions for a foolproof election,
	that any concerned voter could safely and responsibly supervise.</p>

	<p>We should document all the major pitfalls to avoid, and the proper procedure to establish.</p>
	HTML;

$h2_voting_machines = newH2();
$h2_voting_machines['en'] = 'Voting machines';

$div_voting_machines = newSection();
$div_voting_machines['stars']   = 0;
$div_voting_machines['class'][] = '';
$div_voting_machines['en'] = <<<HTML
	<p>It is acceptable to use voting machines and scanners to assist in the electoral process and during the vote count,
	provided that there is a complete paper trail, and that the voters themselves can verify the voting by manually recounting votes in whole or batches.</p>

	<p>The goal is not only to ensure the complete integrity of the election,
	but also to gain the trust of every voter, regardless of their understanding of modern technologies.
	For voters who are having doubts but who can reasonably be persuaded, there is a difference between telling them:
	"Trust us, this machine counts everything accurately",
	and allowing them to see for themselves the votes being counted.</p>

	<p>Elections that are completely automated,
	like voting online or Blockchain-based voting systems,
	may certainly have their uses,
	but for important elections like presidential or legislative elections,
	we should require a method with a voter-verifiable paper trail.	</p>

	<p>The purpose of this section will be to fully document the minimum required protocol for machine-assisted elections,
	the acceptable use and prohibited use of voting machines, scanners, tabulators, etc., during an election.</p>
	HTML;

$body .= printSection($div_stub);
$body .= printSection($div_integrity);

$body .= printH2($h2_voting_machines);
$body .= printSection($div_voting_machines);

include('include/page.php');
