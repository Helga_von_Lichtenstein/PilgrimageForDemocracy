We do not claim ownership of the ideas presented in this project, nor of the text and code used to present them. To the exceptions noted below, we release both the text and the code into the public domain.

For details, check:
src/copyright.php
or:
https://pildem.org/copyright.html

The best ideas for democracy and social justice do not belong to any single person or organization. They belong to all of humanity.

Most of the content in this web site is open source and placed in the public domain. You can share, copy, duplicate, republish, modify all of the most important articles, and add your best ideas, research and policy proposals.

There are however some exceptions for third party material and code. You can check further down this page for details.

You will find very little or no original ideas in this project. Very many good ideas have been thought, spoken, written, published in books, articles or scholarly research, shared in documentaries, movies and videos, shared online in numerous web sites and social networks.

Very many of these good ideas would be beneficial to society and to humanity... if only they were practiced and applied.

We believe it to be a crime against humanity for these ideas to be hidden behind paywalls, made difficultly accessible, burried in arcane books, forgotten, etc.

It is a crime that known good policies are not being put into practice.

Therefore, the Pilgrimage for Democracy and Social Justice aims to gather the best ideas, organize them and make the accessible. Important secondary goals are to educate the public and take collective action to improve our nations' institutions, laws and practices.

We believe in the value of collective intelligence and of the power of teamwork and collective action.

## 3rd party code libraries

### jQuery
For client side scripting, we use jQuery which is released under the MIT license.

### Fomantic-UI
For theming, we use Formantic-UI which is released under the MIT license.
